package tweetservlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

/**
 * Servlet implementation class FriendTweets
 */
@WebServlet("/friendtweets")
public class FriendTweets extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FriendTweets() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DatastoreService dataStore = DatastoreServiceFactory.getDatastoreService();
		ArrayList<String> arrayList = new ArrayList<String>();
		        
		String authorTweet = request.getParameter("author");
		

			Query queue = new Query("Tweet").addSort("visited",Query.SortDirection.DESCENDING); 
		        PreparedQuery pqueue = dataStore.prepare(queue); 
		        		        
		    for (Entity resultSet : pqueue.asIterable()) {   
		    	if(!authorTweet.equals(resultSet.getProperty("author"))) {
		    		
		    	String dateCreated = (String)resultSet.getProperty("DateCreated");
		    	arrayList.add(dateCreated);
		       String tweet = (String)resultSet.getProperty("message");  
		       arrayList.add(tweet);
		       String author = (String)resultSet.getProperty("author");  
		       arrayList.add(author);
		       Long visitedTweet = (Long)resultSet.getProperty("visited");  
		       arrayList.add(visitedTweet.toString());
		    	}
		    }
		       
		    
		 		    response.getWriter().write(arrayList.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
