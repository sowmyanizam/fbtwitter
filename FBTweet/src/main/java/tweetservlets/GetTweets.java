package tweetservlets;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

/**
 * Servlet implementation class GetTweets
 */
@WebServlet("/gettweets")
public class GetTweets extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetTweets() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		


		
		
		String tweetAuthor = request.getParameter("author");
		Query queue = new Query("Tweet"); 
	         System.out.println(tweetAuthor);
			
			ArrayList<String> arrayList = new ArrayList<String>();
			DatastoreService dataStore = DatastoreServiceFactory.getDatastoreService();
		        PreparedQuery pQueue = dataStore.prepare(queue); 
		        
		        
		        
		    for (Entity result : pQueue.asIterable()) {   
		    	if(tweetAuthor.equals(result.getProperty("author"))) {
		    
                        String tweetMessage = (String) result.getProperty("message");  
		       arrayList.add(tweetMessage);

		    	String createdDate = (String) result.getProperty("DateCreated");
		    	arrayList.add(createdDate);
		       		    	}
		    }
		       
		    response.getWriter().write(arrayList.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
