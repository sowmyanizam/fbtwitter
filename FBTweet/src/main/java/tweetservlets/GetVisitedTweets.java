package tweetservlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

/**
 * Servlet implementation class GetVisitedTweets
 */
@WebServlet("/getvisitedtweets")
public class GetVisitedTweets extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetVisitedTweets() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		
		
		String tweetAuthor = request.getParameter("author");
                Query queue = new Query("Tweet").addSort("visited",Query.SortDirection.DESCENDING);
		System.out.println(tweetAuthor);



                        DatastoreService dataStore = DatastoreServiceFactory.getDatastoreService();
		        PreparedQuery pQueue = dataStore.prepare(queue); 
		        ArrayList<String> arrayList = new ArrayList<String>();
		        
		        
		    for (Entity result : pQueue.asIterable()) {   
		    	
		     Long visited = (Long) result.getProperty("visited");
		       
		       arrayList.add(visited.toString());
		    	
		       String tweetMessage = (String) result.getProperty("message");  
		       arrayList.add(tweetMessage);

			String dateCreated = (String) result.getProperty("DateCreated");
		    	arrayList.add(dateCreated);
		       String authorMessage = (String) result.getProperty("author");  
		       arrayList.add(authorMessage);
		      
		    	
		    }
		       		    response.getWriter().write(arrayList.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
