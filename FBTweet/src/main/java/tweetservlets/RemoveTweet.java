package tweetservlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

/**
 * Servlet implementation class RemoveTweet
 */
@WebServlet("/removetweets")
public class RemoveTweet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveTweet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String message = request.getParameter("message");
	System.out.println(message);
	String m = message.substring(1);
	
			DatastoreService dataStore = DatastoreServiceFactory.getDatastoreService();
			Query queue = new Query("Tweet"); 
		        PreparedQuery pQueue = dataStore.prepare(queue); 
		        Key tweetKey = KeyFactory.createKey("Tweet", m);
		        Entity entity=null;
				try {
					entity = dataStore.get(tweetKey);
				} catch (EntityNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        System.out.println(entity.getProperty("DateCreated"));
		        dataStore.delete(tweetKey);
		        
		   
		    
		       
				Query q2 = new Query("Tweet"); 
				String authorTweet = request.getParameter("author");
			System.out.println(authorTweet);
				        PreparedQuery pq2 = dataStore.prepare(q2); 
				        ArrayList<String> arrayList = new ArrayList<String>();
				        
				        
				    for (Entity result : pq2.asIterable()) {   
				    	if(authorTweet.equals(result.getProperty("author"))) {
				    		
				    	String dateCreated = (String) result.getProperty("DateCreated");
				    	arrayList.add(dateCreated);
				       String tweetMessage = (String) result.getProperty("message");  
				       arrayList.add(tweetMessage);
				    	}
				    }
		    
		   
				    response.getWriter().write(arrayList.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
